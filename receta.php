<?php
include 'conexion.php';
$id = 'vacio';
if ( isset( $_GET['id'] ) ) {
  $id = $_GET['id'];
}
?>
<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/express.css">


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">


    <title>EXPRESS ALIMENTOS</title>
  </head>
  <body>
<header class="fixed-top">
  <div class="row justify-content-center">
    <nav  class="navbar navbar-expand-lg navbar-light">
      <a class="navbar-brand" href="index.html">
        <img src="img/logo.png" width="226"  class="d-inline-block align-top" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
           <span> </span>
          <li class="nav-item ">
            <a class="nav-link" href="index.html" >INICIO</a>
          </li>
          <span> </span>
          <li class="nav-item ">
            <a class="nav-link" href="la-empresa.html">LA EMPRESA</a>
          </li>
           <span> </span>
          <li class="nav-item ">
            <a class="nav-link" href="marcas.html">MARCAS</a>
          </li>
           <span> </span>
          <li class="nav-item ">
            <a class="nav-link" href="novedades.html">NOVEDADES</a>
          </li>
           <span> </span>
          <li class="nav-item ">
            <a class="nav-link" href="sucursales.html">SUCURSALES</a>
          </li>
           <span> </span>
          <li class="nav-item active">
            <a class="nav-link" href="recetas.php">RECETAS</a>
          </li>
           <span> </span>
          <li class="nav-item">
            <a class="nav-link" href="contactos.html">CONTACTOS</a>
          </li>
           <span> </span>
        </ul>
      </div>
    </nav>
</div>
</header>
<?php $sql = "SELECT * FROM recetas where id='$id'";
 $result = $db->query($sql);
  if ($result->num_rows > 0) {      
    while($row = $result->fetch_assoc()) {
echo '<div class="receta" style="background-image: url(https://linco.com.py/beta/express/admin/'.$row["foto_dir"].''.$row["foto"].')">';?>
  <div class="container">
    <div class="row justify-content-center">
        <div class="titulo-recetas">
          <center><h4><?php echo $row["titulo"] ?></h4> </center>
        </div>
        <div class="row detalles">
          <div class="col-md-5 col-12">
            TIEMPO DE PREPARACION:  <?php echo $row["tiempo_prep"] ?>
          </div>
          <div class="col-md-4 col-12">
            TIEMPO DE COCCION: <?php echo $row["tiempo_coc"] ?> 
          </div>
          <div class="col-md-3 col-12">
            RINDE: <?php echo $row["rinde"] ?>
          </div>
          </div>
        </div>
        
    </div>
  </div>
</div>

<div class="container pasos">
    <div class="row justify-content-center">

      <div class="col-md-4 col-12">
        <div class="ingredientes">
          <center>
          <p class="sub-receta">Ingredientes</p>
          </center>
          <p> <?php echo $row["ingredientes"] ?>

          </p>
        </div>
      </div>
       <div class="col-md-4 col-12">
        <div class="preparacion">
          <center>
          <p class="sub-receta">Preparación</p>
          </center>
          <p>
            <?php echo $row["preparacion"] ?>
          </p>
        </div>
      </div>
       <div class="col-md-8 col-12">
        <?php echo $row["youtube"] ?>
    </div>
    </div>
</div>
  <?php }} ?>
    <br><br>
          <center>
            <a class=" volver" href="recetas.php">
         Volver 
          </a>
          </center>

<footer>
  <div class="container">

    <div class="row justify-content-center">
      <div class="col-md-2">
       <img src="img/logo-blanco.png">
      </div>
     <div class="col-md-3">
        <p>Casa Matriz</p>
        <p>Acahay 931 c/ tte Villalba; </p>
        <p>B: Mbocajaty</p>
        <p><a style="text-decoration: none; color: white" href="tel:+59521284035">(021) 284-035</a> - <a style="text-decoration: none; color: white" href="tel:+59521284035">(021) 282-474</a> </p>
        <p>Facturacion@expressalimentos.com.py</p>
        <p>pedidos@expressalimentos.com.py</p>
        <div class="linea-footer"></div>    
      </div>
      <div class="col-md-3">
        <p>Suc. Itá</p>
        <p>Calle Francisco Solano López Ruta 1 km 36 </p>
        <p>Posta Gaona - Ita</p>
        <p><a style="text-decoration: none; color: white" href="tel:+595213392168">(021) 339-2168</a></p>
        <p>facturacioncentral@expressalimentos.com.py</p>
        <p></p>
        <div class="linea-footer"></div>
      </div>
      <div class="col-md-3">
        <p>Suc.Ciudad del Este</p>
        <p>Avenida Guarani  c/ Super Carretera Nro. 5</p>
        <p>Puerto Pdte Franco</p>
        <p><a style="text-decoration: none; color: white" href="tel:+59561554045">(061) 554 045</a></p>
        <p>pedidoscde@expressalimentos.com.py</p>
      </div>
      <div class="col-md-1">
        <a href="https://www.facebook.com/expressalimentossrl/" class="facebook">
        <i class="fab fa-facebook-square"></i>
        </a>
      </div>
    </div>
    <center>
    <a href="https://linco.com.py/" target="_blank">
    <img width="56" src="img/linco.svg">
    </a>
    </center>
  </div>

  
</footer >

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


     
  </body>
</html>