<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Receta Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $titulo
 * @property string|null $ingredientes
 * @property string|null $preparacion
 * @property string|null $tiempo_prep
 * @property string|null $tiempo_coc
 * @property string|null $rinde
 * @property string|null $foto_dir
 * @property string|null $foto
 * @property string|null $youtube
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 */
class Receta extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'titulo' => true,
        'ingredientes' => true,
        'preparacion' => true,
        'tiempo_prep' => true,
        'tiempo_coc' => true,
        'rinde' => true,
        'foto_dir' => true,
        'foto' => true,
        'youtube' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
