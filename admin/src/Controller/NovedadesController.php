<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Novedades Controller
 *
 * @property \App\Model\Table\NovedadesTable $Novedades
 *
 * @method \App\Model\Entity\Novedade[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NovedadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $novedades = $this->paginate($this->Novedades);

        $this->set(compact('novedades'));
    }

    /**
     * View method
     *
     * @param string|null $id Novedade id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $novedade = $this->Novedades->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('novedade', $novedade);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $novedade = $this->Novedades->newEntity();
        if ($this->request->is('post')) {
            $novedade = $this->Novedades->patchEntity($novedade, $this->request->getData());
            $novedade->user_id = $this->Auth->user('id');
            if ($this->Novedades->save($novedade)) {
                $this->Flash->success(__('The novedade has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The novedade could not be saved. Please, try again.'));
        }
        $users = $this->Novedades->Users->find('list', ['limit' => 200]);
        $this->set(compact('novedade', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Novedade id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $novedade = $this->Novedades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $novedade = $this->Novedades->patchEntity($novedade, $this->request->getData());
            $novedade->user_id = $this->Auth->user('id');
            if ($this->Novedades->save($novedade)) {
                $this->Flash->success(__('The novedade has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The novedade could not be saved. Please, try again.'));
        }
        $users = $this->Novedades->Users->find('list', ['limit' => 200]);
        $this->set(compact('novedade', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Novedade id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $novedade = $this->Novedades->get($id);
        if ($this->Novedades->delete($novedade)) {
            $this->Flash->success(__('The novedade has been deleted.'));
        } else {
            $this->Flash->error(__('The novedade could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
