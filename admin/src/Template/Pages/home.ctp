<?php

 $usuario=$this->Session->read('Auth.User.id');
 if (empty($usuario)) {
    header("Location: users/login");
    die();
 }else{
 }
 ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">
  <img src="https://linco.com.py/beta/express/img/logo-blanco.png" width="100"  alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <?= $this->Html->link( __('Home').'</a>',
                            ['controller' => 'Pages','action' => 'home',],
                            ['escape' => false, 'class' => 'nav-link']
                        ) ?>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Recetas
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <?= $this->Html->link( __('Listar').'</a>',
                            ['controller' => 'Recetas','action' => 'index',],
                            ['escape' => false, 'class' => 'dropdown-item']
                        ) ?>
          <?= $this->Html->link( __('Crear').'</a>',
                            ['controller' => 'Recetas','action' => 'add',],
                            ['escape' => false, 'class' => 'dropdown-item']
                        ) ?>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Novedades
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <?= $this->Html->link( __('Listar').'</a>',
                            ['controller' => 'Novedades','action' => 'index',],
                            ['escape' => false, 'class' => 'dropdown-item']
                        ) ?>
          <?= $this->Html->link( __('Crear').'</a>',
                            ['controller' => 'Novedades','action' => 'add',],
                            ['escape' => false, 'class' => 'dropdown-item']
                        ) ?>
        </div>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <?= $this->Html->link( __('Salir').'</a>',
                            ['controller' => 'Users','action' => 'logout',],
                            ['escape' => false, 'class' => 'btn btn-outline-danger my-2 my-sm-0']
                        ) ?>
    </form>
  </div>
</nav>