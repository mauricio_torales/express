<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Novedade $novedade
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Novedade'), ['action' => 'edit', $novedade->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Novedade'), ['action' => 'delete', $novedade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $novedade->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Novedades'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Novedade'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="novedades view large-9 medium-8 columns content">
    <h3><?= h($novedade->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $novedade->has('user') ? $this->Html->link($novedade->user->id, ['controller' => 'Users', 'action' => 'view', $novedade->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Titulo') ?></th>
            <td><?= h($novedade->titulo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($novedade->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($novedade->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($novedade->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descripcion') ?></h4>
        <?= $this->Text->autoParagraph(h($novedade->descripcion)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto Dir') ?></h4>
        <?= $this->Text->autoParagraph(h($novedade->foto_dir)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto') ?></h4>
        <?= $this->Text->autoParagraph(h($novedade->foto)); ?>
    </div>
</div>
