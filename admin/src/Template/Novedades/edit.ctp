<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Novedade $novedade
 */
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">
  <img src="https://linco.com.py/beta/express/img/logo-blanco.png" width="100"  alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <?= $this->Html->link( __('Home').'</a>',
                            ['controller' => 'Pages','action' => 'home',],
                            ['escape' => false, 'class' => 'nav-link']
                        ) ?>
      </li>
      <li class="nav-item dropdown ">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Recetas
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <?= $this->Html->link( __('Listar').'</a>',
                            ['controller' => 'Recetas','action' => 'index',],
                            ['escape' => false, 'class' => 'dropdown-item']
                        ) ?>
          <?= $this->Html->link( __('Crear').'</a>',
                            ['controller' => 'Recetas','action' => 'add',],
                            ['escape' => false, 'class' => 'dropdown-item']
                        ) ?>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Novedades
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <?= $this->Html->link( __('Listar').'</a>',
                            ['controller' => 'Novedades','action' => 'index',],
                            ['escape' => false, 'class' => 'dropdown-item']
                        ) ?>
          <?= $this->Html->link( __('Crear').'</a>',
                            ['controller' => 'Novedades','action' => 'add',],
                            ['escape' => false, 'class' => 'dropdown-item']
                        ) ?>
        </div>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <?= $this->Html->link( __('Salir').'</a>',
                            ['controller' => 'Users','action' => 'logout',],
                            ['escape' => false, 'class' => 'btn btn-outline-danger my-2 my-sm-0']
                        ) ?>
    </form>
  </div>
</nav>
<div class="container">
    <?= $this->Form->create($novedade, ['type' => 'file']) ?>
            <div class="form-group">
                <label for="exampleInputEmail1">titulo</label>
                <?php echo $this->Form->control('titulo',['label' => false,'class'=>'form-control']);?>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Descripcion</label>
                <?php echo $this->Form->control('descripcion',['label' => false,'class'=>'form-control']);?>
            </div>
            <div class="form-group">
                <label for="exampleFormControlFile1">Foto</label>
                <input name="" type="file" class="form-control-file" id="foto">
            </div>
        <button type="submit" class="btn btn-primary">Publicar</button>
    <?= $this->Form->end() ?>
</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script>
  $('#foto').click(function(){
      $('#foto').attr('name', 'foto');
  });
</script>
