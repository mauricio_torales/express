<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Novedades'), ['controller' => 'Novedades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Novedade'), ['controller' => 'Novedades', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Recetas'), ['controller' => 'Recetas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Receta'), ['controller' => 'Recetas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Novedades') ?></h4>
        <?php if (!empty($user->novedades)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Titulo') ?></th>
                <th scope="col"><?= __('Descripcion') ?></th>
                <th scope="col"><?= __('Foto Dir') ?></th>
                <th scope="col"><?= __('Foto') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->novedades as $novedades): ?>
            <tr>
                <td><?= h($novedades->id) ?></td>
                <td><?= h($novedades->user_id) ?></td>
                <td><?= h($novedades->titulo) ?></td>
                <td><?= h($novedades->descripcion) ?></td>
                <td><?= h($novedades->foto_dir) ?></td>
                <td><?= h($novedades->foto) ?></td>
                <td><?= h($novedades->created) ?></td>
                <td><?= h($novedades->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Novedades', 'action' => 'view', $novedades->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Novedades', 'action' => 'edit', $novedades->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Novedades', 'action' => 'delete', $novedades->id], ['confirm' => __('Are you sure you want to delete # {0}?', $novedades->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Recetas') ?></h4>
        <?php if (!empty($user->recetas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Titulo') ?></th>
                <th scope="col"><?= __('Ingredientes') ?></th>
                <th scope="col"><?= __('Preparacion') ?></th>
                <th scope="col"><?= __('Tiempo Prep') ?></th>
                <th scope="col"><?= __('Tiempo Coc') ?></th>
                <th scope="col"><?= __('Rinde') ?></th>
                <th scope="col"><?= __('Foto Dir') ?></th>
                <th scope="col"><?= __('Foto') ?></th>
                <th scope="col"><?= __('Youtube') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->recetas as $recetas): ?>
            <tr>
                <td><?= h($recetas->id) ?></td>
                <td><?= h($recetas->user_id) ?></td>
                <td><?= h($recetas->titulo) ?></td>
                <td><?= h($recetas->ingredientes) ?></td>
                <td><?= h($recetas->preparacion) ?></td>
                <td><?= h($recetas->tiempo_prep) ?></td>
                <td><?= h($recetas->tiempo_coc) ?></td>
                <td><?= h($recetas->rinde) ?></td>
                <td><?= h($recetas->foto_dir) ?></td>
                <td><?= h($recetas->foto) ?></td>
                <td><?= h($recetas->youtube) ?></td>
                <td><?= h($recetas->created) ?></td>
                <td><?= h($recetas->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Recetas', 'action' => 'view', $recetas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Recetas', 'action' => 'edit', $recetas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Recetas', 'action' => 'delete', $recetas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recetas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
