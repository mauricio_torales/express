<?php

 $usuario=$this->Session->read('Auth.User.id');
 if (empty($usuario)) { ?>

<style>
#login img{
  margin: 10px 0;
}
#login .center {
  text-align: center;
}

#login .login {
  max-width: 300px;
	margin: 35px auto;
}

#login .login-form{
  padding:0px 25px;
}
</style>
<div id="login" class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="login well well-sm">
        <div class="center">
          <img style="width:300px;" src="https://linco.com.py/beta/express/img/logo.png" alt="logo"> 
        </div>
            <?= $this->Form->create() ?>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input name="email" required="required" class="form-control" placeholder="Usuario" maxlength="255" type="text" id="UserUsername"> 
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
              <input name="password" required="required" class="form-control" placeholder="Password" type="password" id="UserPassword"> 
            </div>
          </div>
               
          <div class="form-group">
            <input class="btn btn-primary btn-lg btn-block" type="submit" value="Ingresar"> 
          </div>
          <?= $this->Form->end() ?>
      </div><!--/.login-->
    </div><!--/.span12-->
  </div><!--/.row-fluid-->
</div><!--/.container-->


<?php   
 }else{
    header("Location: ../");
    die();
 }
 ?>

