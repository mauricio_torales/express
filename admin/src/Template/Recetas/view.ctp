<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Receta $receta
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Receta'), ['action' => 'edit', $receta->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Receta'), ['action' => 'delete', $receta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receta->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Recetas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Receta'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="recetas view large-9 medium-8 columns content">
    <h3><?= h($receta->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $receta->has('user') ? $this->Html->link($receta->user->id, ['controller' => 'Users', 'action' => 'view', $receta->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Titulo') ?></th>
            <td><?= h($receta->titulo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tiempo Prep') ?></th>
            <td><?= h($receta->tiempo_prep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tiempo Coc') ?></th>
            <td><?= h($receta->tiempo_coc) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rinde') ?></th>
            <td><?= h($receta->rinde) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($receta->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($receta->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($receta->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Ingredientes') ?></h4>
        <?= $this->Text->autoParagraph(h($receta->ingredientes)); ?>
    </div>
    <div class="row">
        <h4><?= __('Preparacion') ?></h4>
        <?= $this->Text->autoParagraph(h($receta->preparacion)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto Dir') ?></h4>
        <?= $this->Text->autoParagraph(h($receta->foto_dir)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto') ?></h4>
        <?= $this->Text->autoParagraph(h($receta->foto)); ?>
    </div>
    <div class="row">
        <h4><?= __('Youtube') ?></h4>
        <?= $this->Text->autoParagraph(h($receta->youtube)); ?>
    </div>
</div>
